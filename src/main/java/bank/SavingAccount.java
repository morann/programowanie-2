package bank;

public class SavingAccount extends BankAccount {

    public SavingAccount() {
    }

    public SavingAccount(int balance) {
        super(balance);
    }

    public SavingAccount(int balance, String accountNumber) {
        super(balance, accountNumber);
    }
}
