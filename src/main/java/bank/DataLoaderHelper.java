package bank;

public enum DataLoaderHelper {
    IDENTITY_NUMBER(0),
    FIRST_NAME(1),
    SECOND_NAME(2),
    PESEL(3);

    int i;

    DataLoaderHelper(int value) {
        this.i = value;
    }
}
