package bank;

public class ForeignCurrencyAccount extends BankAccount {

    public ForeignCurrencyAccount() {
    }

    public ForeignCurrencyAccount(int balance) {
        super(balance);
    }

    public ForeignCurrencyAccount(int balance, String accountNumber) {
        super(balance, accountNumber);
    }
}
