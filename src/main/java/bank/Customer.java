package bank;

import org.hibernate.validator.constraints.pl.PESEL;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import javax.validation.constraints.NotBlank;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class Customer {
    @NotBlank
    private String firstName;
    @NotBlank
    private String secondName;
    @PESEL
    private String PESEL;
    private String identityNumber;
    List<BankAccount> customerBankAccounts = new ArrayList<>();

    public Customer(String firstName, String secondName, String PESEL, String identityNumber) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.PESEL = PESEL;
        this.identityNumber = identityNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getIdentityNumber() {
        return identityNumber;
    }

    public String getPESEL() {
        return PESEL;
    }

    public boolean isCustomerValid() {
        final ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory();
        final Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<Customer>> personConstraintViolation = validator.validate(this);
        personConstraintViolation.forEach(element -> System.out.println(element.getMessage()));
        return personConstraintViolation.isEmpty();
    }

    public Customer addAnotherBankAccounts(List<Customer> customers) {
        customers.stream()
                .filter(customer -> this.getPESEL().equals(customer.getPESEL()))
                .forEach(customer -> this.customerBankAccounts.addAll(customer.customerBankAccounts));
        return this;
    }

    public boolean isAnotherCustomer(List<Customer> customers) {
        List<Customer> sameCustomers = customers.stream()
                .filter(customer -> customer.getPESEL().equals(this.getPESEL()))
                .collect(Collectors.toList());
        return sameCustomers.isEmpty();
    }

    public boolean isCustomerOwnerOfAccount(BankAccount account) {
        return this.customerBankAccounts.contains(account);
    }
}
