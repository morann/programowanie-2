package bank;

public class BillingAccount extends BankAccount {

    public BillingAccount() {
    }

    public BillingAccount(int balance) {
        super(balance);
    }

    public BillingAccount(int balance, String accountNumber) {
        super(balance, accountNumber);
    }
}
