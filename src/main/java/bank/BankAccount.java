package bank;

import java.util.List;
import java.util.stream.Collectors;

public class BankAccount {
    int balance;
    String accountNumber;

    public BankAccount() {
    }

    public BankAccount(int balance) {
        this.balance = balance;
    }

    public BankAccount(int balance, String accountNumber) {
        this.balance = balance;
        this.accountNumber = accountNumber;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public boolean isAnotherBankAccount(List<BankAccount> accounts) {
        if (this.accountNumber == null) {
            return true;
        } else {
            List<BankAccount> sameAccounts = accounts.stream()
                    .filter(account -> account.getAccountNumber() != null)
                    .filter(account -> account.accountNumber.equals(this.accountNumber))
                    .collect(Collectors.toList());
            return sameAccounts.isEmpty();
        }
    }
}
