package bank;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static bank.DataLoaderHelper.*;

public class BankCustomersDataLoader {

   private static Map<String, BankAccount> bankAccountPerType = new HashMap<String, BankAccount>() {{
       put("F", new ForeignCurrencyAccount());
       put("B", new BillingAccount());
       put("S", new SavingAccount());
   }};

    List<Customer> createBankCustomers(Path path, Bank bank) {
        List<Customer> customers = new ArrayList<>();
        try {
            customers = Files.readAllLines(path).stream()
                    .map(s -> createCustomer(s, bank))
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return customers;
    }

    private Customer createCustomer(String customerData, Bank bank) {

        String[] data = customerData.split("\\|");
        Customer customer = bank.createCustomer(new Customer(data[FIRST_NAME.i],
                data[SECOND_NAME.i],
                data[PESEL.i],
                data[IDENTITY_NUMBER.i]));
        for (int i = 4; i < data.length; i += 2) {
            String accountType = data[i];
            String balance = data[i + 1];
            BankAccount bankAccount = findBankAccountType(accountType);
            BankAccount customerAccount = bank.createBankAccount(bankAccount, customer);
            customerAccount.setBalance(Integer.parseInt(balance));
        }
        return customer;
    }

    private BankAccount findBankAccountType(String accountType) {
        return bankAccountPerType.get(accountType);
    }
}
