package bank;

import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class BankMain {

    public static void main(String[] args) {
        Bank ING = new Bank("ING Bank Śląski");

        Customer customer_412 = new Customer("Billy", "Holiday", "45010311243", "412");
        Customer customer_000 = new Customer("Sam", "Anonymous", "79022387541", "000");
        Customer customer_123 = new Customer("Sherlock", "Holmes", "54083124654", "123");
        Customer customer_124 = new Customer("Sherlock", "Holmes", "54083124654", "123");
        try {
            customer_412 = ING.createCustomer(customer_412);
            customer_000 = ING.createCustomer(customer_000);
            customer_123 = ING.createCustomer(customer_123);
            customer_124 = ING.createCustomer(customer_124);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }

        BankAccount billing_account_customer_412 = ING.createBankAccount(new BillingAccount(0), customer_412);
        BankAccount foreign_currency_account_customer_412 = ING.createBankAccount(new ForeignCurrencyAccount(0, "1223"), customer_412);
        BankAccount saving_account_customer_412 = ING.createBankAccount(new SavingAccount(0), customer_412);
        BankAccount foreign_currency_account_customer_123 = ING.createBankAccount(new ForeignCurrencyAccount(0), customer_123);

        ING.payOnAccount(1000000, foreign_currency_account_customer_412);
        ING.payOnAccount(1000, billing_account_customer_412);
        ING.withdrawFromAccount(1100, billing_account_customer_412, customer_412);
        ING.payOnAccount(100, billing_account_customer_412);
        ING.withdrawFromAccount(1000, saving_account_customer_412, customer_412);
        ING.payOnAccount(456789, foreign_currency_account_customer_123);
        ING.withdrawFromAccount(1000000, foreign_currency_account_customer_412, customer_123);

        System.out.println("\nBank customers added by DataLoader");
        BankCustomersDataLoader dataLoader = new BankCustomersDataLoader();

        Bank ALIOR = new Bank("Alior Bank");
        Bank SANTANDER = new Bank("Santander Bank");
        Bank MBANK = new Bank("mBank");
        try {
            dataLoader.createBankCustomers(Paths.get("src/main/resources/ALIORcustomers"), ALIOR);
            dataLoader.createBankCustomers(Paths.get("src/main/resources/INGcustomers"), ING);
            dataLoader.createBankCustomers(Paths.get("src/main/resources/MBANKcustomers"), MBANK);
            dataLoader.createBankCustomers(Paths.get("src/main/resources/SANTANDERcustomers"), SANTANDER);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
        ALIOR.customers.forEach(ALIOR::removeEmptyAccounts);
        ING.customers.forEach(ING::removeEmptyAccounts);
        SANTANDER.customers.forEach(SANTANDER::removeEmptyAccounts);
        MBANK.customers.forEach(MBANK::removeEmptyAccounts);

        ALIOR.removeCustomersWithoutAccounts();
        ING.removeCustomersWithoutAccounts();
        SANTANDER.removeCustomersWithoutAccounts();
        MBANK.removeCustomersWithoutAccounts();

        ING.getBankCustomersList();
        ING.getBankAccountsList();
        ING.getCustomerAccountsDescription();
        ALIOR.getBankCustomersList();
        ALIOR.getBankAccountsList();
        ALIOR.getCustomerAccountsDescription();
        MBANK.getBankCustomersList();
        MBANK.getBankAccountsList();
        MBANK.getCustomerAccountsDescription();
        SANTANDER.getBankCustomersList();
        SANTANDER.getBankAccountsList();
        SANTANDER.getCustomerAccountsDescription();

        System.out.println(ING.customers.size());
        System.out.println(ALIOR.customers.size());
        System.out.println(MBANK.customers.size());
        System.out.println(SANTANDER.customers.size());

        System.out.println("_____________________");
        ING.getCustomerAccountsDescription();
        System.out.println("\nING bank takes over Alior bank customers");
        ;
        List<Customer> newINGBankCustomers = ALIOR.customers.stream()
                .filter(customer -> customer.isAnotherCustomer(ING.customers)).collect(Collectors.toList());
        ING.customers.addAll(newINGBankCustomers);
        ALIOR.customers.removeAll(newINGBankCustomers);
        ING.customers.stream()
                .filter(customer -> !customer.isAnotherCustomer(ALIOR.customers))
                .forEach(customer -> customer.addAnotherBankAccounts(ALIOR.customers));
        ALIOR.customers.removeAll(ALIOR.customers);
        ING.getCustomerAccountsDescription();
        System.out.println(ALIOR.customers.isEmpty());

    }
}
