package bank;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Bank {
    String bankName;
    List<Customer> customers = new ArrayList<>();
    List<BankAccount> accounts = new ArrayList<>();

    public Bank(String bankName) {
        this.bankName = bankName;
    }

    public Customer createCustomer(@NotNull Customer customer) throws IllegalArgumentException {
        if (customer.isAnotherCustomer(this.customers) && customer.isCustomerValid()) {
            this.customers.add(customer);
            return customer;
        } else throw new IllegalArgumentException();
    }

    public BankAccount createBankAccount(@NotNull BankAccount bankAccount, @NotNull Customer customer) throws IllegalArgumentException {
        if (bankAccount.isAnotherBankAccount(this.accounts)) {
            customer.customerBankAccounts.add(bankAccount);
            this.accounts.add(bankAccount);
            return bankAccount;
        } else throw new IllegalArgumentException();
    }

    public void removeCustomersWithoutAccounts() {
        this.customers.removeAll(this.customers.stream()
                .filter(customer -> customer.customerBankAccounts.size() == 0)
                .collect(Collectors.toList()));
    }

    public void removeEmptyAccounts(Customer customer) {
        List<BankAccount> emptyAccounts = customer.customerBankAccounts.stream()
                .filter(account -> account.getBalance() == 0)
                .collect(Collectors.toList());
        emptyAccounts
                .forEach(account -> customer.customerBankAccounts.remove(account));
        emptyAccounts
                .forEach(account -> accounts.remove(account));
    }

    public void payOnAccount(int payment, @NotNull BankAccount account) {
        if (payment > 0) {
            account.setBalance(account.getBalance() + payment);
        } else
            System.out.println("You cannot pay " + payment + ".");
    }

    public void withdrawFromAccount(int payment, @NotNull BankAccount account, @NotNull Customer customer) {
        boolean customerIsOwnerOfAccount = customer.isCustomerOwnerOfAccount(account);
        if (payment > 0 && customerIsOwnerOfAccount) {
            if (payment <= account.getBalance()) {
                account.setBalance(account.getBalance() - payment);
            } else {
                System.out.println("Your account balance allows to withdraw " + account.getBalance() + ".");
                account.setBalance(0);
            }
        } else
            System.out.println("You cannot withdraw " + payment + ".");
        if (!customerIsOwnerOfAccount) {
            System.out.println("You are not the owner of " + account.getClass().getSimpleName() +
                    ((account.getAccountNumber() != null) ? (" " + account.getAccountNumber()) : ""));
        }
    }

    public void getBankCustomersList() {
        System.out.println("\nAll " + this.bankName + " customers:");
        this.customers
                .forEach(customer -> System.out.println(customer.getIdentityNumber() + ": " + customer.getFirstName() +
                        " " + customer.getSecondName() + " " + customer.getPESEL()));
    }

    public void getBankAccountsList() {
        System.out.println("\nAll " + this.bankName + " accounts:");
        this.accounts
                .forEach(account -> System.out.println(account.getClass().getSimpleName() + " " + account.getBalance()));
    }

    public void getCustomerAccountsDescription() {
        Map<String, List<BankAccount>> customerPESELToAccounts = this.customers.stream()
                .collect(Collectors.toMap(Customer::getPESEL, customer -> customer.customerBankAccounts));
        System.out.println("\n" + this.bankName + " customers with their accounts:");
        for (Map.Entry<String, List<BankAccount>> entry : customerPESELToAccounts.entrySet()) {
            System.out.println("Customer " + entry.getKey() + ":");
            entry.getValue().forEach(account -> System.out.println(account.getClass().getSimpleName() + " " + account.getBalance()));
        }
    }

}