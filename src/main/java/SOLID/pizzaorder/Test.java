package SOLID.pizzaorder;

public class Test {

    public static void main(String[] args) {

        Address myAddress = new Address("Right Here", "000");
        User me = new User("morann@gmail.com", myAddress);

        Product khachapuri = new Khachapuri();
        Product vegetarianPizza = new VegetarianPizza();
        Product fruitSalad = new FruitSalad();

        OrderService mealOrderService = new MealOrderService();

        mealOrderService.order(me, khachapuri);
        mealOrderService.order(me, vegetarianPizza);
        mealOrderService.order(me, fruitSalad);
    }
}
