package SOLID.pizzaorder;

public class Address {
    private String street;
    private String streetNumber;
    private String localNumber;

    public Address(String street, String streetNumber, String localNumber) {
        this.street = street;
        this.streetNumber = streetNumber;
        this.localNumber = localNumber;
    }

    public Address(String street, String streetNumber) {
        this.street = street;
        this.streetNumber = streetNumber;
    }

    public String getStreet() {
        return street;
    }

    public String getStreetNumber() {
        return streetNumber;
    }

    public String getLocalNumber() {
        return localNumber;
    }

}
