package SOLID.pizzaorder;

public interface OrderService {

    boolean order(User user, Product product);

}
