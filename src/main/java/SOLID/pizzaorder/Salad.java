package SOLID.pizzaorder;

public class Salad implements Product {
    @Override
    public String getProductName() {
        return Salad.this.getClass().getSimpleName();
    }
}
