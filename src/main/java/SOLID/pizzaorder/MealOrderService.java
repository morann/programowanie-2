package SOLID.pizzaorder;

public class MealOrderService implements OrderService {

    @Override
    public boolean order(User user, Product product) {
        System.out.println("Request sent from: " + user.getEmail());
        System.out.println("Ordering " + product.getProductName() + " on address "
                + user.address.getStreet() + " " + user.address.getStreetNumber() + " "
                + ((user.address.getLocalNumber() != null) ? ("/" + user.address.getLocalNumber() + " ") : " "));

        return true;
    }
}
