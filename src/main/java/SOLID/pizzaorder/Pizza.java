package SOLID.pizzaorder;

public class Pizza implements Product {

    @Override
    public String getProductName() {
        return Pizza.this.getClass().getSimpleName();
    }
}
