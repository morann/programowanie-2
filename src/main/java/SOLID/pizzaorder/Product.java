package SOLID.pizzaorder;

public interface Product {

    String getProductName();

}
