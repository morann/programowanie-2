package SOLID.pizzaorder;

public class User {

    String email;
    Address address;

    public User(String email, Address address) {
        this.email = email;
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

}
