package SOLID.isp;

import java.util.ArrayList;
import java.util.Collection;

public class FileLogger implements Logger {
    @Override
    public void writeMessage(String message) {
        // write message to file
    }

    @Override
    public Collection<String> getMessages() {
        //get message from logger file
        return new ArrayList<>();
    }
}
