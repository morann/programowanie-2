package SOLID.isp.fix;

public interface Logger {
    void writeMessage(String message);
}
