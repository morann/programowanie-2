package SOLID.isp.fix;

import java.util.ArrayList;
import java.util.Collection;

public class FileLogger implements ReadableLogger {
    @Override
    public Collection<String> getMessages() {
        // get messages from logger file
        return new ArrayList<>();
    }

    @Override
    public void writeMessage(String message) {
        // write message to file
    }
}
