package SOLID.lsp.fix;

public class ReadFile implements ReadFileOperation {
    @Override
    public byte[] read() {
        return new byte[0];
    }
}
