package SOLID.lsp.fix;

public interface WriteFileOperation {

    void write(byte[] data);
}
