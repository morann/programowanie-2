package SOLID.lsp.fix;

public interface ReadFileOperation {
    byte[] read();
}
