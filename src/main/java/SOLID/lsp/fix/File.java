package SOLID.lsp.fix;

public class File implements WriteFileOperation, ReadFileOperation {
    @Override
    public byte[] read() {
        return new byte[0];
    }

    @Override
    public void write(byte[] data) {

    }
}
