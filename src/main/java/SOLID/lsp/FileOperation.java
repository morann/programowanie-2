package SOLID.lsp;

public interface FileOperation {

    byte[] read();

    void write(byte[] data);
}
