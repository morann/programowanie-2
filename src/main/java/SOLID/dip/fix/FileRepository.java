package SOLID.dip.fix;

public class FileRepository implements Repository{

     @Override
     public void saveTask(String task) {
     }

     @Override
     public void deleteTask(int taskId) {
     }
}
