package SOLID.dip.fix;

public class Test {

    public static void main(String[] args) {

        Repository repository = new FileRepository();
        TaskService taskService = new TaskService(repository);

        taskService.addTask("Task 1");

        repository = new DBRepository();
        TaskService taskService1 = new TaskService(repository);
        taskService1.removeTask(1);

        // na początku FileRepo, potem DBRepo
        taskService = new TaskService(repository);
    }
}
