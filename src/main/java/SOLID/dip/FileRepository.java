package SOLID.dip;

public class FileRepository {

    public void saveTask(String task) {}

    public void deleteTask(int taskId) {}
}
