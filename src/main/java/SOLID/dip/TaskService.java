package SOLID.dip;

public class TaskService {
    FileRepository repository = new FileRepository();

    public void addTask(String task) {
        repository.saveTask(task);
    }

    public void removeTask(int taskId) {
        repository.deleteTask(taskId);
    }
}
