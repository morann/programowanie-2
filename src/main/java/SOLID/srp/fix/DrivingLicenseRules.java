package SOLID.srp.fix;

public class DrivingLicenseRules {

    private static final int DRIVING_LICENCE_REQUIRED_AGE = 18;
    private static final int DRIVING_LICENCE_REQUIRED_AGE_WITH_ADULT = 16;

    public static boolean canGetDrivingLicence(Person person) {
        // ekstrahować 18 poza klase i wczytywać jako konfiguracje
        return person.getAge() > DRIVING_LICENCE_REQUIRED_AGE;
    }

}
