package SOLID.ocp;

public enum LogDestination {
    CONSOLE,
    DB;
}
