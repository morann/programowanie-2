package SOLID.ocp.fix;

public interface MessageLogger {

    void log(String message) throws Exception;
}
