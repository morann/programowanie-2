package searchEngine;

import java.util.Comparator;

public class BookByAuthor implements Comparator<Book> {

    @Override
    public int compare(Book book1, Book book2) {
        return book1.getCreator().compareTo(book2.getCreator());
    }
}
