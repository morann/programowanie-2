package searchEngine;

public class CreationNotFoundException extends Exception {
    public CreationNotFoundException() {
        super("searchEngine.Creation not found");
    }
}
