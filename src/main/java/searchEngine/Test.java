package searchEngine;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Test {
    public static void main(String[] args) {
        Movie tibet = new Movie("Tibet", "A", LocalDate.of(1954, 8, 23));
        Movie spiderman = new Movie("Spiderman", "B", LocalDate.of(1999, 12, 13));
        Movie tiffanyBreakfast = new Movie("Tiffany Breakfast", "C", LocalDate.of(1987, 1, 1));
        Movie manInTheMirror = new Movie("Man in the mirror", "D", LocalDate.of(2000, 5, 14));
        Movie something = new Movie("Something", "E", LocalDate.of(1933, 3, 3));

        List<Movie> movies = new ArrayList<>();
        movies.add(tibet);
        movies.add(spiderman);
        movies.add(tiffanyBreakfast);
        movies.add(manInTheMirror);
        movies.add(something);

        Book ashtanga = new Book("Ashtanga", "Martin", LocalDate.of(2005, 12, 3));
        Book shantaram = new Book("Shantaram", "Gregory", LocalDate.of(2008, 12, 3));
        Book kwiat_pustyni = new Book("Kwiat pustyni", "Lui", LocalDate.of(2008, 12, 3));
        List<Book> books = new ArrayList<>();
        books.add(ashtanga);
        books.add(shantaram);
        books.add(kwiat_pustyni);

        SearchEngine searchMovie = new SearchEngine(movies);
        System.out.println(searchMovie.printCreationInfoOptional("Tibet"));
        System.out.println(searchMovie.printCreationInfoOptional("ZZZ"));
        System.out.println(searchMovie.printCreationInfoException("Man in the mirror"));

        SearchEngine searchBook = new SearchEngine(books);
        System.out.println(searchBook.printCreationInfoException("ZZZ"));
        System.out.println(searchBook.printCreationInfoException("Ashtanga"));

        System.out.println("__________________");
        System.out.println("Movies unsorted");
        movies.forEach(element -> System.out.println(element.getDescriptionData()));
        movies.forEach(System.out::println); // bierze toString, jeśli go nie ma wyświetla nazwe klasy i adres
        System.out.println("\nMovies sorted");
        movies.sort(Comparator.comparing(Movie::getTitle));
        movies.forEach(element -> System.out.println(element.getDescriptionData()));
        System.out.println("\nMovies sorted by premiere date");
        Collections.sort(movies, new MovieByPremiereDate());
        movies.forEach(element -> System.out.println(element.getDescriptionData()));
        System.out.println("\nMovies sorted natural by title"); // za pomocą interfejsu Comparable, implementujemy
        Collections.sort(movies);
        movies.forEach(element -> System.out.println(element.getDescriptionData()));

        System.out.println("__________________");
        System.out.println("Books unsorted");
        books.forEach(element -> System.out.println(element.getDescriptionData()));
        books.forEach(System.out::println);
        System.out.println("\nBooks sorted by author");
        books.sort(Comparator.comparing(book -> book.author));
        books.forEach(element -> System.out.println(element.getDescriptionData()));
        System.out.println("\nBooks sorted by author and premiere date");
        books.sort(Comparator.comparing(Book::getCreator).thenComparing(Book::getPremiereDate));
        books.forEach(element -> System.out.println(element.getDescriptionData()));
        System.out.println("\nBooks sorted by author, external comparator");
        Collections.sort(books, new BookByAuthor());
        books.forEach(element -> System.out.println(element.getDescriptionData()));
        System.out.println("\nBooks sorted natural by title");
        Collections.sort(books);
        books.forEach(element -> System.out.println(element.getDescriptionData()));

    }
}
