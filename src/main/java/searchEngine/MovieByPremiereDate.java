package searchEngine;

import java.util.Comparator;

public class MovieByPremiereDate implements Comparator<Movie> {     // komparator zewnętrzny, w osobnej klasie, implementuje Comparator
    @Override
    public int compare(Movie movie1, Movie movie2) {
        return movie1.getPremiereDate().compareTo(movie2.getPremiereDate());
    }
}
