package searchEngine;

import java.util.List;
import java.util.Optional;

public class SearchEngine <E extends Creation> {
    private List<E> creations;

    public SearchEngine(List<E> creations) {
        this.creations = creations;
    }

    private E searchCreation(String title) {
        return creations.stream()
                .filter(creation -> creation.getTitle().equalsIgnoreCase(title))
                .findAny()
                .orElse((E) Creation.DEFAULT_CREATION);
    }

    public String printCreationInfo(String title) {
        E creation = searchCreation(title);
        if (Creation.DEFAULT_CREATION == creation) {         // obiekt domyślny dla klasy, obiekt statyczny, nie new, == to samo miejsce w pamięci
            return "Creation not found";                    // nie equals
        }
        return creation.getTitle() + " " + creation.getCreator() + " " + creation.getPremiereDate();
    }

    private Optional<E> searchCreationOptional(String title) {
        return creations.stream()
                .filter(creation -> creation.getTitle().equalsIgnoreCase(title))
                .findAny();
    }

    public String printCreationInfoOptional(String title) {
        Optional<E> creation = searchCreationOptional(title);
        if (creation.isPresent()) {
            return creation.get().getTitle() + " " + creation.get().getCreator() + " " + creation.get().getPremiereDate();
        } else return "Creation not found";
    }

    private E searchCreationException(String title) throws CreationNotFoundException {
        return creations.stream()
                .filter(seek -> seek.getTitle().equals(title))
                .findAny()
                .orElseThrow(CreationNotFoundException::new);
    }

    public String printCreationInfoException(String title) {
        try {
            E creation = searchCreationException(title);
            return creation.getTitle() + " " + creation.getCreator() + " " + creation.getPremiereDate();
        } catch (CreationNotFoundException e) {
            return "Creation not found";
        }
    }
}


