package searchEngine;

import java.time.LocalDate;

public class Book extends Creation implements Comparable<Book> {
    String title;
    String author;
    LocalDate premiereDate;

    public Book(String title, String author, LocalDate premiereDate) {
        this.title = title;
        this.author = author;
        this.premiereDate = premiereDate;
    }

    @Override
    public String getTitle() {
        return this.title;
    }

    @Override
    public String getCreator() {
        return this.author;
    }

    @Override
    public LocalDate getPremiereDate() {
        return this.premiereDate;
    }

    @Override
    public String getDescriptionData() {
        return this.title + " " + this.author + " " + this.premiereDate;
    }

    @Override
    public String toString() {
        return "Book{" +
                "title='" + title + '\'' +
                ", author='" + author + '\'' +
                ", premiereDate=" + premiereDate +
                '}';
    }

    @Override
    public int compareTo(Book book) {
        return this.title.compareTo(book.getTitle());
    }
}
