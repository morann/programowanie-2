package bank;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BankTest {
    private Bank testBank = new Bank("TestBank");
    private Customer testCustomer = new Customer("John", "Watson", "85082567874", "1");
    private BankAccount testAccount = new BankAccount(1000000, "111");
    private Customer nullCustomer;
    private BankAccount nullAccount;

    @BeforeEach
    public void setTestBank() {
        testBank.createCustomer(testCustomer);
        testBank.createBankAccount(testAccount, testCustomer);
    }

    @Test
    public void shouldNotCreateTheSameCustomer() {
        assertThrows(IllegalArgumentException.class, () -> testBank.createCustomer
                (new Customer("Sherlock", "Holmes", "85082567874", "9")));
        assertEquals(1, testBank.customers.size());
    }

    @Test
    public void shouldNotAcceptNullCustomer() {
        assertThrows(IllegalArgumentException.class, () -> testBank.createCustomer(null));
    }

    @Test
    public void shouldNotCreateTheSameAccount() {

        assertThrows(IllegalArgumentException.class, () -> testBank.createBankAccount(new BankAccount(900, "111"),
                testCustomer));
        assertThrows(IllegalArgumentException.class, () -> testBank.createBankAccount(new BankAccount(0, "111"),
                new Customer("Sherlock", "Holmes", "85082567874", "9")));
        assertEquals(1, testCustomer.customerBankAccounts.size());
        assertEquals(0, new Customer("Sherlock", "Holmes", "85082567874", "9")
                .customerBankAccounts.size());
    }

    @Test
    public void shouldNotPayOnNotExistingAccount() {
        assertThrows(IllegalArgumentException.class, () -> testBank.payOnAccount(1000, nullAccount));
    }

    @Test
    public void shouldNotWithdrawFromAccountOfNullCustomer() {
        assertThrows(IllegalArgumentException.class, () -> testBank.withdrawFromAccount(1000, testAccount, nullCustomer));
    }

    @Test
    public void shouldNotWithdrawFromAccountOfAnotherCustomer() {
        testBank.withdrawFromAccount(1000000, testAccount,
                new Customer("Kate", "Thief", "45010311243", "000"));
        assertEquals(1000000, testAccount.getBalance());
    }

    @Test
    public void shouldWithdrawProperAmount() {
        testBank.withdrawFromAccount(1000000, testAccount, testCustomer);
        assertEquals(0, testAccount.getBalance());
        testBank.withdrawFromAccount(1000, testAccount, testCustomer);
        assertEquals(0, testAccount.getBalance());
        testBank.withdrawFromAccount(-1000, testAccount, testCustomer);
        assertEquals(0, testAccount.getBalance());
    }
}