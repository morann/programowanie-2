package bank;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTest {
    Customer test_1_Customer = new Customer("Catherine", "Spy", "45010311243", "1");
    Customer test_2_Customer = new Customer("Ann", "Spy", "450103", "1");

    @Test
    public void isCustomerValid() {
        assertTrue(test_1_Customer.isCustomerValid());
        assertFalse(test_2_Customer.isCustomerValid());
    }

}